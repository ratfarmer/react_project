import React, {Component} from 'react';

class ProductInput extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.props.onProductChange(e.target.value);
    }

    render() {
        const pid = this.props.pid;
        return (
            <fieldset>
                <legend>Enter Model Number</legend>
                <input value={pid}
                       onChange={this.handleChange} />
            </fieldset>
        );
    }
}

export default ProductInput;