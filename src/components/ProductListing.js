import React, {Component} from 'react';

class ProductListing extends Component {
    constructor(props) {
        super(props);
        this.state = productTable;
    }

    render(){
        const productModel = this.props.productModel === 'undefined' ? '' : this.props.productModel;
        const xword = productModel !== '' ? productModel:'';

        return (
            <div>
                Model Number Selected: {xword}
                <table>
                    {productTableHeader}
                    <tbody>
                    {this.state.products.map((p) => {
                        if(p.model === xword){
                            return(
                                <tr key={p.model}>
                                    <td key={p.model}>{p.model}</td>
                                    <td key={p.img}><img alt={p.img} src={p.img} /></td>
                                    <td key={p.name}>{p.name}</td>
                                    <td key={p.price}>{p.price}</td>
                                </tr>
                            )
                        }
                        if(xword.length === 0){
                            return(
                                <tr key={p.model}>
                                    <td key={p.model}>{p.model}</td>
                                    <td key={p.img}><img alt={p.img} src={p.img} /></td>
                                    <td key={p.name}>{p.name}</td>
                                    <td key={p.price}>{p.price}</td>
                                </tr>
                            )}
                        //Return something if there is nothing
                        //Although null is very close to nothing.....
                        return( null );
                    })}
                    </tbody>
                </table>
            </div>
        );
    }
}

const productTable = {
    products: [
        {
            name: 'Product A',
            model: '212',
            price: '10.00',
            img: 'https://www.jhpfasteners.com/thumb.php?img=images%2Fproducts%2FRN_ACD.jpg&w=200&h=200',
            status: true
        },
        {
            name: "Product B",
            model: '213',
            price: '12.50',
            img: 'https://www.jhpfasteners.com/thumb.php?img=images%2Fproducts%2FJPSN_CD.jpg&w=200&h=200',
            status: true
        },
        {
            name: 'Product C',
            model: '214',
            price: '14.80',
            img: 'https://www.jhpfasteners.com/thumb.php?img=images%2Fproducts%2FSCHL_D.JPG&w=200&h=200',
            status: true
        },
        {
            name: 'Product D',
            model: '215',
            price: '14.75',
            img: 'https://www.jhpfasteners.com/thumb.php?img=images%2Fproducts%2FJPSN_CD.jpg&w=200&h=200',
            status: true
        },
        {
            name: 'Product E',
            model: '216',
            price: '18.80',
            img: 'https://www.jhpfasteners.com/thumb.php?img=images%2Fproducts%2FJPSN_CD.jpg&w=200&h=200',
            status: true
        },
        {
            name: 'Product F',
            model: '217',
            price: '11.80',
            img: 'https://www.jhpfasteners.com/thumb.php?img=images%2Fproducts%2FRN_ACD.jpg&w=200&h=200',
            status: true
        },
        {
            name: 'Product G',
            model: '218',
            price: '17.20',
            img: 'https://www.jhpfasteners.com/thumb.php?img=images%2Fproducts%2FSCHL_D.JPG&w=200&h=200',
            status: true
        },
        {
            name: 'Product H',
            model: '219',
            price: '18.80',
            img: 'https://www.jhpfasteners.com/thumb.php?img=images%2Fproducts%2FRN_ACD.jpg&w=200&h=200',
            status: true
        },
        {
            name: 'Product I',
            model: '220',
            price: '57.80',
            img: 'https://www.jhpfasteners.com/thumb.php?img=images%2Fproducts%2FSCHL_D.JPG&w=200&h=200',
            status: true
        }

    ]
};

const productTableHeader = (
    <thead>
    <tr>
        <td>Model Number</td>
        <td>Image</td>
        <td>Name</td>
        <td>Price</td>
    </tr>
    </thead>
);

export default ProductListing;