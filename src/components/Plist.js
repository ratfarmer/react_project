import React, { Component } from "react";
import ProductInput from './ProductInput';
import ProductListing from './ProductListing';

class Plist extends Component {
    constructor(props){
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {word: ''};
    }

    handleChange(word) {
        this.setState({word});
    }

    render() {
        const word = this.state.word;

        return (
            <div id="temp-check">
                <ProductInput
                    pid={word}
                    onProductChange={this.handleChange} />
                <ProductListing
                    productModel={word} />
            </div>
        );
    }
}

export default Plist;