import React, { Component } from 'react';
import Plist from './components/Plist';
import './App.css';

class App extends Component {
  render() {
      return (
          <div id="Search-box">
              <Plist />
          </div>
      );
  }
}

export default App;
